Feature: Login
  I want to login with existing user
  Scenario Outline: Scenario: Successfull Login with an existing user
    Given I am already navigated to Login Page
    And I enter <username> valid username
    And I enter <password> valid password
    When I click on submit button
    Then login should be done successfully
    And I should be navigated to Login Home page
   Examples: 
      | username           | password |
      | xyz@rediffmail.com |     123 | 
      | abc@rediffmail.com |     456 |
  Scenario Outline: Scenario: Un-successfull Login with an existing user
    Given I am already navigated to Login Page
    And I enter <username> invalid username
    And I enter <password> invalid password
    When I click on submit button
    Then the "<message>" should be dispalyed
   Examples: 
      | username        | password | message |
      | xyz@redmail.com |     123  | message |
      | abc@rediffmail  |     456  | message |
