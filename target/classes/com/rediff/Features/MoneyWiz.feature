Feature: Getting top losers and gainers,BSE and NSE indices
 
  @tag1
  Scenario: To find top losers and gainers
    Given I am already navigated to MONEYWIZ page
    When I visit the MARKET section 
    Then list of top gainers should appear
    When I click LOSERS button
    Then list of top "<losers>" should be displayed

 @tag2
    Scenario Outline: BSE Indices
    Given I am already navigated to MONEYWIZ page
    And I visit MARKET LIVE section
    When I check for the "<BSE>" index
    Then I should get the information of that particular BSE
    Examples: 
      | BSE |   
      | S&P BSE 100    |  
      | S&P BSE 200    |  
      | S&P BSE 500    |  
      | S&P BSE MidCap |  
      
  Scenario Outline: NSE Indices
    Given I am already navigated to MONEYWIZ page
    And I visit MARKET LIVE section
    When I check for the "<NSE>" index
    Then I should get the information of that particular NSE
    Examples: 
      | NSE     |   
      | NIFTY 50    |  
      | NIFTY IT    |  
      | NIFTY NEXT 50 | 
      | NIFTY BANK   | 
      | NIFTY 500    |   
      
      
  Scenario Outline: BSE Indices
    Given I am already navigated to MONEYWIZ page
    And I visit MARKET LIVE section
     When I check for any "<index>"  index
    Then I should get the information of that particular NSE
    Examples: 
      | NSE     |   
      | NIFTY 50    |  
      | NIFTY IT    |  
      | NIFTY NEXT 50 | 
      | NIFTY BANK   | 
      | NIFTY 500    |   
      
 