package com.rediff.FeatureTestSteps;

import org.openqa.selenium.WebDriver;

import com.rediff.Help.DriverFactory;
import com.rediff.Pages.MoneyWizPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MoneyWizFeatureTest extends DriverFactory {	
WebDriver driver;
MoneyWizPage money;
MoneyWizFeatureTest()
{
	money=new MoneyWizPage(driver);
}
@Given("^I am already navigated to MONEYWIZ page$")
public void i_am_already_navigated_to_MONEYWIZ_page()  {
money.clickOnMoneyLink();   
}

@When("^I visit the MARKET section$")
public void i_visit_the_MARKET_section() {
	}

@Then("^list of top gainers should appear$")
public void list_of_top_gainers_should_appear() {
	money.maxCurrentPrice();
	
    
	}

@When("^I click LOSERS button$")
public void i_click_LOSERS_button() {
	
    
	}

@Then("^list of top \"(.*?)\" should be displayed$")
public void list_of_top_should_be_displayed(String arg1) {
	
    
	}

@Given("^I visit MARKET LIVE section$")
public void i_visit_MARKET_LIVE_section()  {
	
    
	}

@When("^I check for the \"(.*?)\" index$")
public void i_check_for_the_index(String arg1)  {
	
    
	}
@Then("^I should get the information of that particular BSE$")
public void i_should_get_the_information_of_that_particular_BSE()  {
	
    
	}

@Then("^I should get the information of that particular NSE$")
public void i_should_get_the_information_of_that_particular_NSE()  {
	
    
	}

@When("^I check for any \"(.*?)\"  index$")
public void i_check_for_any_index(String arg1)  {
	
    
	}



}
