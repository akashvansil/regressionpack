package com.rediff.FeatureTestSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginFeatureTest {
	
	@Given("^I am already navigated to Login Page$")
	public void i_am_already_navigated_to_Login_Page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter xyz@rediffmail\\.com valid username$")
	public void i_enter_xyz_rediffmail_com_valid_username() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter (\\d+) valid password$")
	public void i_enter_valid_password(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^I click on submit button$")
	public void i_click_on_submit_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^login should be done successfully$")
	public void login_should_be_done_successfully() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I should be navigated to Login Home page$")
	public void i_should_be_navigated_to_Login_Home_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter abc@rediffmail\\.com valid username$")
	public void i_enter_abc_rediffmail_com_valid_username() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter xyz@redmail\\.com invalid username$")
	public void i_enter_xyz_redmail_com_invalid_username() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter (\\d+) invalid password$")
	public void i_enter_invalid_password(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^the \"(.*?)\" should be dispalyed$")
	public void the_should_be_dispalyed(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I enter abc@rediffmail invalid username$")
	public void i_enter_abc_rediffmail_invalid_username() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}
