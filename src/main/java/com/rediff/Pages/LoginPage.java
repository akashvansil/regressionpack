package com.rediff.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.log4testng.Logger;

public class LoginPage {
	WebDriver driver;
	private static Logger logger = Logger.getLogger(LoginPage.class);
	
	private By Login_SignIn      =       By.cssSelector("a[class='signin']");
    private By Login_username    =       By.id("#login1");
    private By Login_password    =       By.id("#password");
    private By Login_Submit      =       By.cssSelector("input[type='submit']");
   
    public void setUsername(String username){
    	driver.findElement(Login_username).sendKeys(username);
    }
    public void setPassword(String password){
    	driver.findElement(Login_password).sendKeys(password);
    	
    }
    public void submitLogin(){
    	driver.findElement(Login_Submit).click();
    }

}
