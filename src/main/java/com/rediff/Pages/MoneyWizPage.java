package com.rediff.Pages;



import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.log4testng.Logger;

public class MoneyWizPage {
	WebDriver driver;
	private static Logger logger = Logger.getLogger(LoginPage.class);
	
	private By Money_Page      =       By.cssSelector("a[class='moneyicon relative']");
    private By Money_Losers    =     By.cssSelector("span[class='blue curhand gainerloser']");
   // private By Money_gainersPercent   =By.cssSelector("div#bseGainerDiv [class=row_GL_col3]");
    private By Money_moreGainers     = By.cssSelector("#div_bse_gainer .alignR");
    private By Money_moreLosers      = By.cssSelector("#div_bse_loser .alignR");
    private By Money_moreBSEindices  = By.cssSelector("#div_bseindices .ht5+.alignR a");
    private By Money_moreNSEindices  = By.cssSelector(".hmnseindicestable+ .ht5+ .alignR a:last-child");
    private By Money_currentPrice    = By.xpath("//*[@class='dataTable']//tr/td[4]");
    private By Money_row             = By.xpath("//*[@class='dataTable']//tr");
    private By Money_table           = By.xpath("//*[@class='dataTable']/tbody");

 public MoneyWizPage(WebDriver driver) {
		this.driver=driver;
	}
public void maxCurrentPrice() 
 {
	 int row_size=driver.findElements(Money_row).size();
   	 int col_size=driver.findElements(Money_currentPrice).size();
	 for(int row=1;row<row_size;row++)
	 {
		 for(int column=0;column<col_size;column++)
		 {
			System.out.println("//*[@class='dataTable']//tr["+row+"]/td["+column+"]");
		 }
	 }
	
 }
 public void getDynamicTableData()
 {
	WebElement table=driver.findElement(Money_table);
	List<WebElement> rows=table.findElements(By.tagName("tr"));
	for(int rnum=1;rnum<rows.size();rnum++)
	{
		List<WebElement> cols=rows.get(rnum).findElements(By.tagName("td"));
		System.out.println("number of columns:"+cols.size());
		for(int cnum=0;cnum<cols.size();cnum++)
		{
			System.out.println(cols.get(cnum).getText());
		}
	}
	 
 }
 
public void clickOnMoneyLink(){
	driver.findElement(Money_Page).click();
}
public void getTopGainers(String CompanyName){
	
	
}
public void getTopLosers(String CompanyName){
	
}
public void sectoralIndices(){
	
}
public void bseIndices(){
	
}
public void nseIndices(){
	
}

}
