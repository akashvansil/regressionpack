package com.rediff.Help;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.rediff.Util.SeleniumUtil;

public class DriverFactory {
	
	public static WebDriver driver;
	public static Properties prop;
	
	public static WebDriver getDriver()
	{
		String browserName=prop.getProperty("browser");
		if(browserName.equals("chrome"))
		{
			String chromePath=prop.getProperty("chromePath");	 
			System.setProperty("webdriver.chrome.driver",chromePath);
			driver=new ChromeDriver();
		}
		else if("firefox".equals(browserName))
		{
			driver=new FirefoxDriver();
		}
		else
		{
			try
			{
				throw new Exception("browser not supported");
			}
			catch(Exception e)
			{
				System.out.println("Requested browser is not supported");	
			}
		}
		driver.manage().timeouts().implicitlyWait(SeleniumUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
		return driver;
	}
/*	
/public static void TakeScreenShot(String currTestName) {
            
            if ("true".equals(TestProperty.TAKE_SCREENSHOTS)) {
                  String testName = addDate(currTestName);

                  @SuppressWarnings("deprecation")
                  String outputDIR = TestNG.getDefault().getOutputDirectory();
                  final String newFileNamePath = outputDIR + "\\ScreenShot\\" + currTestName + "\\" + testName + ".jpg";

                  final File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                  
                  try {
                        
                        FileUtils.copyFile(scrFile, new File(newFileNamePath));
                        logger.info("The Screenshot is saved for- " + testName);
                        
                  } catch (IOException e) {
                        logger.error("The File is not found, in the takeScreenShot method of the driverFactory...");
                        e.printStackTrace();
                  }
            }
      }
*/
		
	

}
