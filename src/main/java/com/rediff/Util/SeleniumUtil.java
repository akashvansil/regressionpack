package com.rediff.Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Random;

//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.rediff.Help.DriverFactory;

public class SeleniumUtil extends DriverFactory {
	public static String TESTDATA_SHEET_PATH="D:\\Eclipse_Workspace\\Framework\\src\\main\\java\\com\\gl\\qa\\"
			+ "testdata\\rediff_data.xlsx";
	        WebDriver driver;
	        public static long IMPLICIT_WAIT=10;
	       // static Workbook book;
	        //static Sheet sheet;
	        public SeleniumUtil(WebDriver driver)
	        {
	            this.driver = driver;
	        }
	        
	        /*public Object[][] LoginData(String sheetName) {
	        	FileInputStream file=null;
	        	try{
	        		file=new FileInputStream(TESTDATA_SHEET_PATH);
	        	}
	        	catch(FileNotFoundException e){
	        		e.printStackTrace();
	        	}
	        	try{
	        		book=WorkbookFactory.create(file);
	        	}catch (IOException e){
	        		e.printStackTrace();
	        	}catch (org.apache.poi.openxml4j.exceptions.InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	sheet=book.getSheet(sheetName);
	        	Object[][] data=new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
	        	for(int i=0;i<sheet.getLastRowNum();i++){
	        		for(int k=0;k<sheet.getRow(0).getLastCellNum();k++){
	        			data[i][k]=sheet.getRow(i+1).getCell(k).toString();
	        		}
	        		
	        	}
	        	return data;
	        	
	        }*/

	        public void Click(By usernameClick , WebDriver driver)
	        {
	            try
	            {
	                driver.findElement(usernameClick).click();
	            }
	            catch (Exception e)
	            {
	                System.out.printf("not clickable", e.getStackTrace());
	            }
	        }


	        /*public void SendKeys(string str1, string str2)
	        {
	            driver.FindElement(By.XPath(str1)).SendKeys(str2); 
	        }*/
	        public void GetTitle(WebDriver driver)
	        {
	            try
	            {
	                String title = driver.getTitle();
	                System.out.println(title);
	            }
	            catch (Exception e)
	            {
	            	System.out.println("Failed to retrieve title");
	            }
	        }

	        public void GetPageSource(WebDriver driver)
	        {
	            try
	            {
	                String PgSource = driver.getPageSource();
	                System.out.println(PgSource);
	            }
	            catch (Exception e)
	            {
	            	System.out.println("Failed to get page source");
	            }
	        }



	        public void FindElements(String xpath, int num, WebDriver driver)
	        {
	            try
	            {
	                List<WebElement> list = driver.findElements(By.xpath(xpath));
	                int count = list.size();
	                for (int i = 0; i <= count; i++)
	                {
	                    if (i == num)
	                    {
	                        list.get(i);
	                        break;
	                    }
	                }
	            }
	            catch (NoSuchElementException e)
	            {
	            	System.out.printf("Element not found", e.getStackTrace());
	            }
	        }

	       /* public void ClickRadioBtn(String xpath, int num, WebDriver driver)
	        {
	            try
	            {
	                List<WebElement> list = driver.findElements(By.xpath(xpath));
	                int count = list.Count();
	                for (int i = 0; i <= count; i++)
	                {
	                    if (i == num)
	                    {
	                        list.ElementAt(i).Click();
	                        break;
	                    }
	                }
	            }
	            catch (Exception e)
	            {
	                Console.WriteLine("Element ot clickable", e.StackTrace);
	            }
	        }

	        public void SelectCheckBox(string xpath, int num, IWebDriver driver)
	        {
	            try
	            {
	                IList<IWebElement> list = driver.FindElements(By.XPath(xpath));
	                int count = list.Count();
	                for (int i = 0; i <= count; i++)
	                {
	                    if (i == num)
	                    {
	                        list.ElementAt(i).Click();
	                        break;
	                    }
	                }
	            }
	            catch (ElementNotSelectableException)
	            {
	                Console.WriteLine("Element not Selectable");
	            }
	        }

	        public void DropdownSelectByIndex(string xpath1, string xpath2, int index, IWebDriver driver)
	        {
	            try
	            {
	                driver.FindElement(By.XPath(xpath1)).Click();
	                IWebElement element = driver.FindElement(By.XPath(xpath2));
	                SelectElement select = new SelectElement(element);
	                select.SelectByIndex(index);
	            }
	            catch (ElementNotSelectableException)
	            {
	                Console.WriteLine("Failed to select element in Dropdown");
	            }

	        }

	        public void WaitForElemenToBePresent(IWebDriver driver, String xpath)
	        {
	            try
	            {
	                IWebElement element = driver.FindElement(By.XPath(""));
	                if (element.Displayed)
	                {
	                    Console.WriteLine("Element found.");
	                }
	                else
	                {
	                    Console.WriteLine("Element not found");
	                }
	            }
	            catch (ElementNotVisibleException)
	            {
	                Console.WriteLine("Element not present");
	            }
	        }

	        public void SelectKeys(string xpath, string text, IWebDriver driver)
	        {
	            try
	            {

	                SelectElement select = new SelectElement(driver.FindElement(By.XPath(xpath)));
	                select.SelectByText(text);
	            }
	            catch (Exception e)
	            {
	                Console.WriteLine("Failed to select", e.StackTrace);
	            }
	        }*/

	        public void SendKeysToWebElement(By usernameTextbox, String text, WebDriver driver)
	        {
	            try
	            {
	               // driver.findElement(By.).sendKeys(text);
	            }
	            catch (Exception e)
	            {
	            	System.out.println("Failed to enter the text in textbox");

	            }
	        }
/*
	        public bool IsElementEnabled(string xpath, IWebDriver driver)
	        {
	            try
	            {
	                bool s = driver.FindElement(By.XPath(xpath)).Enabled;

	            }
	            catch (OpenQA.Selenium.NoSuchElementException)
	            {
	                Console.WriteLine("Element not enabled");
	                return false;
	            }
	            return true;
	        }

	        public bool IsElementDisplayed(string xpath, IWebDriver driver)
	        {
	            try
	            {
	                bool s = driver.FindElement(By.XPath(xpath)).Displayed;
	            }
	            catch (OpenQA.Selenium.ElementNotVisibleException)
	            {
	                Console.WriteLine("Element not displayed");
	                return false;
	            }
	            return true;
	        }

	        public void Hover(IWebDriver driver, string xpath)
	        {
	            IWebElement element = driver.FindElement(By.XPath(xpath));
	            Actions hover = new Actions(driver);
	            hover.Click().MoveToElement(element).Perform();
	        }

	        public string GetTextfromWebelement(string xpath, IWebDriver driver)
	        {
	            string text = null;
	            try
	            {
	                text = driver.FindElement(By.XPath(xpath)).Text;
	            }
	            catch (Exception e)
	            {
	                Console.WriteLine("Unable to get text from WebElement", e.StackTrace);
	            }
	            return text;
	        }

	        public void WaitForElementToBeVisible(By locator, IWebDriver driver)
	        {
	            try
	            {
	                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
	                Func<IWebDriver, bool> waitForElement = new Func<IWebDriver, bool>((IWebDriver Web) =>
	                {
	                    Console.WriteLine(Web.FindElement(locator));
	                    return true;
	                });

	                wait.Until(waitForElement);
	            }
	            catch (NoSuchElementException)
	            {
	                Console.WriteLine("Element not visible");
	            }
	        }


	        public void WaitForElementToBeVisibleAndReturn(IWebDriver driver, By locator)
	        {
	            try
	            {
	                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
	                Func<IWebDriver, IWebElement> waitForElement = new Func<IWebDriver, IWebElement>((IWebDriver Web) =>
	                {
	                    IWebElement element = driver.FindElement(locator);
	                    return element;
	                });
	            }
	            catch (ElementNotVisibleException)
	            {
	                Console.WriteLine("Element not visible");
	            }
	        }

	        public void WaitForPageLoad(IWebDriver driver)
	        {

	        }
	        /*  public void WaitForElementTextToBeVisible(By locator, IWebDriver driver)
	          {
	              try
	              {
	                  WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
	                  wait.Until(driver => driver.FindElement(locator));
	              }
	              catch (NoSuchElementException)
	              {
	                  Console.WriteLine("Element not present");
	              }

	          }

	          public bool WaitForElementToDisappear(By locator, IWebDriver driver)
	          {
	              WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
	              bool element =wait.Until(driver => driver.FindElement(locator)).Displayed;
	              if (element == true)
	              {
	                  Console.WriteLine("Element is still present");
	              }
	              else
	              {
	                  Console.WriteLine("Element disappears!!");
	              }
	              return element;
	          }

	          public ArrayList<Integer> GenerateUniqueRandomNumber(IWebDriver driver)
	          {
	              return;
	          }

	          public Boolean CheckTextPresentInPage(string text, IWebDriver driver)
	          {
	              bool textState = false;
	              if (driver.FindElement(By.TagName("body")).Text.Contains(text))
	              {
	                  textState = true;
	              }
	              return textState;
	          }

	          public void SelectByVisibleText(By xpath, string text, IWebDriver driver)
	          {
	              SelectElement dropdown = new SelectElement(driver.FindElement(xpath));
	              dropdown.SelectByText(text);
	          }

	          public void ScrollToElement(By xpath, IWebDriver driver)
	          {

	              IWebElement element = driver.FindElement(xpath);

	          }

	          public void Sleep()
	          {
	              try
	              {
	                  Thread.Sleep(5000);
	              }
	              catch (ThreadInterruptedException e)
	              {
	                  Console.WriteLine("", e.StackTrace);
	              }
	          }

	          public IWebElement Element(IWebDriver driver, By locator)
	          {
	              IWebElement element = driver.FindElement(locator);
	              return element;
	          }
	          public void SwitchToFrame(IWebDriver driver, By locator)
	          {
	              driver.SwitchTo().Frame(Element(driver, locator));

	          }

	          public void SwitchtoDefaultContent(IWebDriver driver)
	          {
	              driver.SwitchTo().DefaultContent();
	          }*/

	          public void SelectRandomDropdown(WebElement element)
	          {
	           
	              Select objSelect = new Select(element);
	              List<WebElement> weblist = objSelect.getOptions();
	              int size = weblist.size();
	              Random num = new Random();
	              int iSelect = num.nextInt(size);
	              objSelect.selectByIndex(iSelect);
	              System.out.println(element.getAttribute("value"));
	          }
/*
	          public void SelectRandomCheckBox(By chkbox, IWebDriver driver)
	          {

	              IList<IWebElement> oCheckBox = driver.FindElements(chkbox);
	              Random num = new Random();
	              int iCnt = num.Next(oCheckBox.Count());
	              oCheckBox.get(iCnt).click();
	              oCheckBox.ElementAt(iCnt).Click();

	          }


	          public void ClickRandomDropdown(By dropDown, String type, IWebDriver driver)
	          {
	              String dropDownText = "no";
	              Random randomOption = new Random();
	              if (type.Contains("Select"))
	              {
	                  SelectElement randomDrop = new SelectElement(driver.FindElement(dropDown));
	                  int option = randomOption.Next(randomDrop.Options.Count() - 1);
	                  randomDrop.SelectByIndex(option);
	                  dropDownText = driver.FindElement(dropDown).Text;
	              }
	              else if (type.Contains("List"))
	              {
	                  IList<IWebElement> randomDrop = driver.FindElements(dropDown);
	                  int option = randomOption.Next(randomDrop.Count() - 1);
	                  randomDrop.get(option).click();
	                  dropDownText = randomDrop.get(option).getText();
	                  randomDrop.ElementAt(option).Click();
	                  dropDownText = randomDrop.ElementAt(option).Text;
	              }
	              else
	              {
	                  Console.WriteLine("Please check the dropdown parameters entered");
	              }
	              Console.WriteLine("Clicked on the " + dropDownText + " dropdown option.");
	          }



	          public static void SelRadioOption(String name, int i, IWebDriver driver)
	          {
	              try 
	              {
	                  IList<IWebElement> RadButtonList = driver.FindElements(By.Name(name));
	                  RadButtonList.ElementAt(i).Click();
	                  Console.WriteLine("User select the radio option");

	              }
	              catch (Exception e)
	              {
	                  Console.WriteLine("Failed to click on element",e.StackTrace);

	              }

	          }*/
}



