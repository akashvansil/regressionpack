package com.rediff.Util;
import org.apache.log4j.Logger;



import com.lh.utils.PropertyFileReader;
import com.lh.utils.UtilityMethods;
import java.util.Properties;
 

/**
 * <h2>Test Properties are defined in this class!</h2>
 * This class reads the configuration properties
 * of the automation run.
 * <p>
 * <b>Note:</b> All the test properties should be defined in the 
 * PROPERTY files in the project
 * 
 * @author akash.vansil
 * @version 1.0
 * @since  2014-11-17
 */

public class TestProperty {

	    /** propObj is an instance of PropertyFileReader class */
	    final static PropertyFileReader prop= new PropertyFileReader();
	    /** Instance of the properties */
	    final static Properties propObj = prop.returnProperties("automation");
	    /** BASEURL contains the URL which is used for test Automation */
	    public static final String BASEURL = propObj.getProperty("BASEURL");
	    public static final String PORTALURL = propObj.getProperty("PORTALURL");
	    public static final String STEPSURL = propObj.getProperty("STEPSURL");
	    public static final String WEIGHTURL = propObj.getProperty("WEIGHTURL");
	    public static final String PHYSICALURL = propObj.getProperty("PHYSICALURL");
	    public static final String NUTRIURL = propObj.getProperty("NUTRIURL");
	    public static final String GENERALURL = propObj.getProperty("GENERALURL");
	    public static final String HEARTYURL = propObj.getProperty("HEARTYURL");
	    
	    public static final String BASE_URL = propObj.getProperty("BASE_URL");
	    public static final String BROWSER = propObj.getProperty("BROWSER");
	    
	     
	    /** ZIPFILE_REPORT_NAME contains the name of the zipped report */
	    public static final String ZIPFILE_REPORT_NAME =  propObj.getProperty("ZIPFILE_REPORT_NAME");
	    /** CONFIG_AUTOMATION contains relative path of the configuration file      */
	    public static final String CONFIG_AUTOMATION = "/test/resources/automation.properties";
	    /** WAITING_TIME contains the webDriverWait time */
	    
	    public static final int WAITING_TIME =  Integer.parseInt(propObj.getProperty("WAITING_TIME"));
	    /** ELEMENT_WAIT_TIME is the time in milli seconds the driver waits for to proceed in HelperWebDriverEventListener */
	    public static final int ELEMENT_WAIT_TIME =  Integer.parseInt(propObj.getProperty("ELEMENT_WAITTIME"));
	    /** ELEMENT_WAIT_TIME is the time in seconds the driver waits for to proceed in HelperWebDriverEventListener */
	    public static final int ELEMENT_POLL_TIME =  Integer.parseInt(propObj.getProperty("ELEMENT_POLLTIME"));
	    
	    /** CLIENT_PORTAL_TITLE contains the title of the client portal under test */
	    public static final String CLIENT_PORTAL_TITLE             = propObj.getProperty("CLIENT_PORTAL_TITLE");
	    /** ADMIN_LOGIN_TITLE  contains the title of the admin portal under test */
	    public static final String ADMIN_LOGIN_TITLE             = propObj.getProperty("ADMIN_LOGIN_TITLE");
	    public static final String ADMIN_HOME_TITLE             = propObj.getProperty("ADMIN_HOME_TITLE");
	    public static final String ADMIN_VIEW_RESULT             = propObj.getProperty("ADMIN_VIEW_RESULT");
	    public static final String ADMIN_ADD_ONTRACK            = propObj.getProperty("ADMIN_ADD_ONTRACK");
	    
	    /** EMAIL_REPORT_TO contains the email addresses to which the email is sent on test completion */
	    public static final String EMAIL_REPORT_TO                 = propObj.getProperty("EMAIL_REPORT_TO");
	    /** THREAD_SLEEP contains the value in long for the Thread.sleep(THREAD_SLEEP) stmt */
	    private static String THREAD_SLEEP_STRING = propObj.getProperty("THREAD_SLEEP_STRING").trim();
	    /** RETRY_COUNT indicates the number of times a failed test case re executed  */
	    public static final int RETRY_COUNT = Integer.parseInt(propObj.getProperty("RETRY_COUNT"));

	 

	    public static long THREAD_SLEEP = Long.valueOf(THREAD_SLEEP_STRING).longValue();
	    public static final String TESTDATA_WORKBOOK =  propObj.getProperty("XLS_DATA").trim();
	    public static final String TAKE_SCREENSHOTS = propObj.getProperty("TAKE_SCREENSHOTS").trim().toLowerCase();
	    
	    public static final String CLIENTURL = UtilityMethods.extractPartialClientURL(propObj.getProperty("BASEURL"));
	    
	    /** Logger for the HealthAssessment class */
	    private static Logger logger                             = Logger.getLogger(TestProperty.class);
	    
	    /**
	     * Overriding the default constructor
	     */
	    protected TestProperty (){
	    	   logger.debug("Creating instance of the TestProperty class");
	    }
	}
	
}
