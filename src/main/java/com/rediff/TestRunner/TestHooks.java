package com.rediff.TestRunner;

import org.openqa.selenium.WebDriver;

import com.rediff.Help.DriverFactory;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class TestHooks extends DriverFactory {
WebDriver driver;	
public TestHooks(){}

@Before
public void init(){
	driver=getDriver();
}

@After
public void tearDown(Scenario sce){
	if(driver!=null && sce.isFailed()){
		//TakeScreenshot();
		driver.quit();
		driver=null;
	}
}


}
