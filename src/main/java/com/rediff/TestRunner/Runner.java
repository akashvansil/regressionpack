package com.rediff.TestRunner;
import org.junit.runner.RunWith;          
import cucumber.api.CucumberOptions;               
import cucumber.api.junit.Cucumber;                

@RunWith(Cucumber.class)
@CucumberOptions(
		/*features = "src/test/resources/functionalTests",
		glue= {"stepDefinitions"},
		plugin = { "pretty" },
		monochrome = true*/
		
		features = "src/main/java/com/rediff/Features",
		glue= {"src/test/java/com/rediff/FeatureTestSteps"},
		plugin = { "pretty" },
		monochrome = true
		)
public class Runner /*extends AbstractCucumberTestNG */{
	
	
	
}

